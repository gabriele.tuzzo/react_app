export default function Table() {
    return (
        <>
            <table class="table">
                <tr>
                    <td class="text-center fldesc-wb">
                        Serj Tankian
                    </td>
                    <td class="text-center fldesc-wb">
                        Daron Malakian
                    </td>
                    <td class="text-center fldesc-wb">
                        Shavo Obadjian
                    </td>
                </tr>
                
                <tr>
                    <td class="text-center">
                        <Link to="/"><img  class="img-responsive rounded-circle" src="https://i.scdn.co/image/ab676161000051745e304e5c1968ca518011749d" style="height: 120px; width: 180px;display:inline-block" /></Link>
                    </td>  
                    <td class="text-center">
                        <Link to="/"><img class="img-responsive" src="https://upload.wikimedia.org/wikipedia/commons/8/8d/DM-photo.jpg" style="height: 120px; width: 180px;display:inline-block" /></Link>
                    </td> 
                    <td class="text-center">
                        <Link to="/"><img class="img-responsive" src="https://imageio.forbes.com/specials-images/dam/imageserve/468777488/960x0.jpg?height=517&width=711&fit=bounds" style="height: 120px; width: 180px;display:inline-block" /></Link>
                    </td> 
                </tr>

            </table>
        </>
    )
}